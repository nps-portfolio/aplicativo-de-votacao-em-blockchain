# Dapp de Votação con contrato em blockchain pública

Esse repositório é de uma aplicação descentralizada (Dapp) para votação digital utilizando a blockchain pública de testes da Ethereum (Ropsten).

---------------------------------------------------------------------------------------
 PRÉ-REQUISITOS PARA IMPLEMENTAÇÃO
---------------------------------------------------------------------------------------
NPM

Nessa implementação a versão utilizada foi a 5.6.0.

NODE

Nesse exemplo a versão do node utilizada foi 8.11.3.
Caso esteja com dificuldades para configurar, recomendo desinstalar o nodejs e o npm e instalar novamente utilizando o NVM.

O pacote do ethereumjs-tx não funciona com o node. Foi necessário importá-lo manualmente.

TRUFFLE

Versões utilizadas
     - Truffle v4.1.13 (core: 4.1.13)
     - Solidity v0.4.24 (solc-js)

INFURA

- Conta na Infura.io.

---------------------------------------------------------------------------------------
 COMO FOI UTILIZADO ESSE DAPP
---------------------------------------------------------------------------------------
Essa implementação foi utilizada como sistema de votação para escolha do nome da Biblioteca do IFTM - Campus Paracatu no ano de 2018. Serviu também para Trabalho de Conclusão de Curso em Análise e Desenvolvimento de Sistemas na mesma instituição. 

Breve resumo

- Após os testes, o contrato foi enviado para a blockchain da Ethereum;
- Em uma hora (limite do contrato) foram gerado os hashes válidos para votação;
- Os hashes foram enviados para os emails dos eleitores (alunos e servidores) em formato de link para que pudessem votar;
- Após a votação foram atualizadas as páginas do frontend para apresentar os resultados;

Telas do Dapp

![VOTAÇÃO ](https://i.ibb.co/VHD4H3f/votacao-teala.png)

![RESULTADO ](https://i.ibb.co/vPT6x31/resultado-votacao-tela.png)

---------------------------------------------------------------------------------------
 REFERÊNCIAS PARA DESENVOLVIMENTO
---------------------------------------------------------------------------------------

Baseado no trabalho de Gregory from Dapp University!
FONTE: http://www.dappuniversity.com/articles/the-ultimate-ethereum-dapp-tutorial
