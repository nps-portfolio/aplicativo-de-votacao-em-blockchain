pragma solidity ^0.4.24;

// Título: Contrato para votação de eleição do nome da Biblioeca do IFTM - Campus Paracatu

// Desenvolvido por: Nataniel P. Santos (natanielsa@gmail.com)

// Descrição: Esse Smart Contract foi desenvolvido com o objetivo de ser utilizaddo para realizar
// a votação da escolha do nome da biblioteca. Sua utlização foi pensada na facilidade do eleitor,
// que receberá o código de acesso por email e realizará seu voto que ficará armazenado na blockchain
// pública da ethereum (versão de teste Ropsten).

// Observação: Esse contrato será utilizado em caráter de testes e apesar de suprir as necessidades
// do caso de uso em que será usado, não atende a todas as recomendações de segurança exigidas para um smart contracts.

//  VANTAGENS

// - Armazenamento dos votos de forma inalterável (na rede Ropsten)
// - Auditoria dos votos (qualquer eleitor, após a votação e divulgação de resultado, poderá verificar o contrato e os votos realizados)
// - Transparência (todos os votos ficam disponíveis na internet e podem ser facilmente verificados )
// - Praticidade (os eleitores realizam o voto pela internet, com código único recebido por email)
// - Segurança ( o código de votação é enviado para o email do eleitor)
// - Confiabilidade ( após a execução do contrato, seu código não pode ser ALTERADO OU ATUALIZADO. Dessa forma, todo processo de votação é
// realizado, organizado e finalizado por um algoritmo, sem interferência humana de qualquer natureza )

contract Eleicao{

    // Modelo de candidato
    struct Candidato {
        uint id;
        string nome;
        uint voteQuantidade;
    }


    // Eventos
    event eventoConfirmaVoto(uint index_candidatoId);

    // Determina o número máximo de eleitores
    uint private NUM_MAX_ELEITORES = 1400;

    // Determina o tempo de duração da votação
    uint public dataFinal;
    uint public dataCriacao;

    // Endereço de quem critou o contrato
    // address public criador;

    // Armazena as contas de eleitores
    mapping(bytes32 => bool) public eleitores;

    // Armazena os hashes com permissão de votar
    mapping(bytes32 => bool ) private hashEleitoresMapping; // lembrar de deixar privado

    bytes32[] public hashEleitoresArray;

    // Determinar a quantidade de hashes
    uint public contador = 0;

    // Armazena as informações sobre os candidatos
    mapping(uint => Candidato) public candidatos;

    // Armazena a quatidade de eleitores
    uint public eleitoresQuantidade;

    // Armazena a quantidade de candidatos
    uint public candidatosQuantidade;

    // Armazena a quantidade de eleitores que já votaram
    uint public jaVotaram;

    // Esse é o constructor do contrato que é executado uma uńica vez ( na criação do contrato ), e que determina as condições para a votação
    constructor () public {

        // Adiciona os candidatos que podem receber votos
        addCandidato("[1] - Anísio Spínola Teixeira");
        addCandidato("[2] - Branca Adjuto Botelho");
        addCandidato("[3] - José Leite Lopes");

        // Determina a data da criação do contrato (timestamp)
        dataCriacao = now;

        // Determina o prazo final para votação, sendo de 3 dias após a criação do contrato
        dataFinal = dataCriacao + 6 * 1 days;

        // O endereço do criador do contrato
        // criador = msg.sender;

    }

    // // Essa função permite que o contrato receba ether e que uma vez recebido seja impossível retirar
    // function () payable public{

    // }

    // // Função modificadora que restringe o acesso à algumas funções apenas para o criador do contrato
    // // Para uma auditoria poderia, por exemplo, permitir acesso só a um grupo de usuários
    // modifier somenteCriador {

    //     require(msg.sender == criador);
    //     _;
    // }

    // Função privada que adiciona os candidatos na eleição
    function addCandidato (string _name)  private {

        candidatosQuantidade ++;
        candidatos[candidatosQuantidade] = Candidato(candidatosQuantidade, _name, 0);

    }

    // funcão que gera os hashes no contrato
     // As chaves de permissão de votação foram geradas separadamente,
    // em blocos de 50 por causa do preço do gas.
    // Para garantir a segurança e transparência, elas só podem ser adicionadas
    // em até uma hora após a criação do contrato

    function geraHash(string _string) public{
        // limita o tempo que essa função pode ser usada
        require(now <= dataCriacao + 1 hours);

        // limita o número de hashes válidos que podem ser gerados
        require(contador < NUM_MAX_ELEITORES);

        for (uint i = 1; i <= 50 ; i++){
            bytes32 _hash = keccak256("eleicaoiftm",abi.encodePacked(i,now,_string,contador));
            hashEleitoresArray.push(_hash);
            hashEleitoresMapping[_hash] = true;
            contador++;
        }
    }

    function votar(uint _candidatoId, bytes32 _hashEleitor) public {

        // Verifica se ainda está no prazo para votar
        require(now <= dataFinal);

        // valida o candidato
        require(_candidatoId > 0 && _candidatoId <= candidatosQuantidade);

         // impede que o hash nulo seja usado para votação
        eleitores[0x00] = true;

        // hash validação
        require(hashEleitoresMapping[_hashEleitor]);

        // verifica se o _hashEleitor já foi usado para votar
        require(!eleitores[_hashEleitor]);

        // informa que o _hashEleitor já votou
        eleitores[_hashEleitor] = true;

        // atualiza a quandidade de votos do canditato
        candidatos[_candidatoId].voteQuantidade ++;

        // incrementa a quantidade de eleitores que votaram
        jaVotaram++;

        //gatilho do evento
        emit eventoConfirmaVoto(_candidatoId);

    }
}
