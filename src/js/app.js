App = {

  web3Provider: null,
  contracts: {},
  account: '0x0', //FIXME sem necessidade de atribuir um endereço inicial

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {

    App.web3Provider = new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/API-KEY-INFURA');
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON("Eleicao.json", function(eleicao) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.Eleicao = TruffleContract(eleicao);
      // Connect provider to interact with contract
      App.contracts.Eleicao.setProvider(App.web3Provider);

      App.listenForEvents();

      return App.render();


    });
  },

  render: function() {

    var electionInstance;
    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    //RECUPERA OS VALORES DA URL
    var url = window.location.search.replace("?", "");
    var items = url.split("&");
    $('#conhecaCandidatos').append('<a href="https://eleicao-dapp.herokuapp.com/candidatos.html?hash&' + items[1]+'" > Conheça os candidatos </a>');
    $('#hashEleitor').attr('value', items[1]);
    // FIM DE RECUPERA

    //GERA OS HASH USANDO A FUNÇÃO ESPECIFICA

    // Verifica se o hash passado pela url é válido (primeira verificação)

    var valido = false;

      if(items[1] != "0x0"){
        valido = true;
      }

    // FIM DA VERIFICAÇÃO DA VALIDADE DO HASH

    if(valido){
      console.log("Pode votar");

      // CARREGA DADOS DO CONTRATO
      App.contracts.Eleicao.deployed().then(function(instance) {

        $('#smarctContractEndereco').html('<a href="https://ropsten.etherscan.io/address/' + instance.address + '">' + instance.address + '</a>');

        //PEGA OS hashes
        electionInstance = instance;

        instance.jaVotaram().then(function(e) {
          qtdJaVotaram.html("<h3>Acompanhe:  " + e + " já votaram. </h3>");
        });

        return electionInstance.candidatosQuantidade();

      }).then(function(qtdCandidatos) {

        var candidatesResults = $("#candidatesResults");
        candidatesResults.empty();

        var candidatesSelect = $('#candidatesSelect');
        candidatesSelect.empty();

        for (var i = 1; i <= qtdCandidatos; i++) {

          electionInstance.candidatos(i).then(function(candidate) {
            var id = candidate[0];
            var name = candidate[1];
            var voteCount = candidate[2];

            // Renderiza resultados
            var candidateTemplate = "<tr><th>" + id + "</th><td>" + name + "</td><td>" + voteCount + "</td></tr>";

            candidatesResults.append(candidateTemplate);

            // Renderiza opções de voto
            var candidateOption = "<option value='" + id + "' >" + name + "</ option>";

            candidatesSelect.append(candidateOption);

          });
        }

        return electionInstance.eleitores(items[1]);

      }).then(function(hasVoted) {
        // PROIBE O VOTO REPETIDO
        if (hasVoted) {
          $('form').hide();
          $('#topo').hide();
          $('footer').hide();
          $('#procedimento').hide();
          $('#votou').show();

        }

        loader.hide();
        content.show();

      }).catch(function(error) {
        console.warn(error);
      });

    }else{
      $('#invalido').show();
      $('#topo').hide();
      $('#footer').hide();
      $('#content').hide();
      $('#procedimento').hide();
      $('#carregamento').hide();
      $('#loader').hide();
      $('#form').hide();
    };

    //FIM DO GERA HASH

      },

  castVote: function() {

    $('#procedimento').hide();
    $('#topo').hide();
    $('#footer').hide();

    var candidateId = $('#candidatesSelect').val();
    var hashEleitor = $('#hashEleitor').val();
    var hashRecebido = '0x0';

    App.contracts.Eleicao.deployed().then(function(instance) {

      //ENVIA O VOTO USANDO UMA ÚNICA CONTA
      var account1 = '0x42cab505acd927b2dd3423056d7004a3e321dbbc';
      var account2 = instance.address;

      account1 = web3.toChecksumAddress(account1);
      account2 = web3.toChecksumAddress(account2);

      const privateKey1 = new EthJS.Buffer.Buffer('PRIVA-KEY1', 'hex') //TODO A chave privada deve ser protegida

      //PEGA O VALOR DA ÚLTIMA TRANSAÇÃoptimize
      //TODO O nonce causa problemas na hora de votos simultâneos. Encontrar um jeito de corrigir.
      web3.eth.getTransactionCount(account1, (err, txCount) => {
        console.log(txCount);
        const txObject = {
          nonce: web3.toHex(txCount),
          valor: web3.toHex(0),
          gasLimit: web3.toHex(3500000),
          gasPrice: web3.toHex(web3.toWei("10", "gwei")),
          to: web3.toChecksumAddress(account2),
          from: web3.toChecksumAddress(account1),
          data: instance.contract.votar.getData(candidateId, hashEleitor),
          chainId: '0x03'
        }

        // assinar a transação
        const tx = new EthJS.Tx(txObject)
        tx.sign(privateKey1)

        const serializedTx = tx.serialize()
        const raw = '0x' + serializedTx.toString('hex')
        console.log(raw);

        // enviar a transação
        web3.eth.sendRawTransaction(raw, (err, txHash) => {
          $('#hash').append('<div id="container" align="center"><div class="alert-warning" style="padding: 20px"><h3 align="center">Votação encerrada!</h3><p><br><h4>Essa votação já foi encerrada. Clique no botão abaixo para ver o resultado: <br><br>' +
            '<a class="btn btn-primary" href="http://eleicao-dapp.herokuapp.com/resultados.html?hash&HASH-GERADO-NO-CONTRATO" role="button">RESULTADO</a>' +
          '</h4><br></div>' +
          '</div>');
        });

        setTimeout(function(){
          window.location.reload();
        },45000);


      })
      // fim de enviar transacao

      //FIM DO ENVIA Voto

      return instance.eleitoresQuantidade();

      //@params result Variável de retorno que precisa ser melhor implementada
    }).then(function(result) {
      // Espera os votos serem atualizados
      $("#content").hide();
      $("#loader").hide();

    }).catch(function(err) {
      console.error(err);
    });
  },

  pegaHashes: function(){
    App.contracts.Eleicao.deployed().then(function(instance){

      for (var i = 0; i <= 1400; i++){

          $('#codigosAtivos').append('<br>Prazo finalizado para executar essa ação');

    };
    });
  },

  //OUVIR EVENTO DO CONTRATO PARA EXECUTAR AÇÕES PARA O usuário
  // TODO Precisa ser melhor implementado e usado o recurso de eventos na aplicação
  listenForEvents: function() {

    App.contracts.Eleicao.deployed().then(function(instance) {

      instance.eventoConfirmaVoto({}, {
        fromBlock: 0,
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("Voto confirmado!", event);
        if(!error){
          $("#hash").hide();
          $("#votou").show();
        }
      //  window.location.reload();
      //App.render();
      });
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
