require('dotenv').config();
const HDWalletProvider = require("truffle-hdwallet-provider");
var teste = require('ethereumjs-tx');

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  Buffer: require('buffer'),
  BN: require('ethereumjs-util').BN,
  RLP: require('ethereumjs-util').rlp,
  Tx: require('ethereumjs-tx'),
  Util: require('ethereumjs-util'),

  networks: {
    development: {
      host: "127.0.0.1",
      network_id: "15" // Match any network id
    },
    ropsten:{
      provider: function(){
        return new HDWalletProvider(
          process.env.MNEMONIC,
          process.env.INFURA_API_KEY
          )
      },
      gas: 2100000,
      gasPrice: 1000000000,
      network_id: 3
    }
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
}
