var Eleicao = artifacts.require("./Eleicao.sol");

contract ("Eleicao", function(accounts){

  it("Verifica se tem dois candidatos", function(){
    return Eleicao.deployed().then(function(instance) {
      return instance.qtdCandidatos();
    }).then(function(count){
      assert.equal(count, 2);
    });
  });

  it("verificar eleitores", function() {
    return Eleicao.deployed().then(function(instance) {
      electionInstance = instance;
      candidatoId = 1;
      return electionInstance.votar(candidatoId, { from: accounts[0] });
    }).then(function(receipt) {
      return electionInstance.eleitores(accounts[0]);
    }).then(function(voted) {
      assert(voted, "O eleitor já votou!");
      return electionInstance.candidatos(candidatoId);
    }).then(function(candidate) {
      var voteCount = candidate[2];
      assert.equal(voteCount, 1, "Aumenta o voto do candidato");
    })
  });

  it("Verifica candidatos inválidos", function() {
    return Eleicao.deployed().then(function(instance) {
      electionInstance = instance;
      return electionInstance.votar(99, { from: accounts[1] })
    }).then(assert.fail).catch(function(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      return electionInstance.candidatos(1);
    }).then(function(candidate1) {
      var voteCount = candidate1[2];
      assert.equal(voteCount, 1, "O candidato 1 não recebeu voto");
      return electionInstance.candidatos(2);
    }).then(function(candidate2) {
      var voteCount = candidate2[2];
      assert.equal(voteCount, 0, "O candidato 2 não recebeu voto");
    });
  });

  it("Verifica se permite votos duplicados", function() {
    return Eleicao.deployed().then(function(instance) {
      electionInstance = instance;
      candidateId = 2;
      electionInstance.votar(candidateId, { from: accounts[1] });
      return electionInstance.candidatos(candidateId);
    }).then(function(candidate) {
      var voteCount = candidate[2];
      assert.equal(voteCount, 1, "Primeiro voto aceito");
      // Try to vote again
      return electionInstance.votar(candidateId, { from: accounts[1] });
    }).then(assert.fail).catch(function(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      return electionInstance.candidatos(1);
    }).then(function(candidate1) {
      var voteCount = candidate1[2];
      assert.equal(voteCount, 1, "candidate 1 did not receive any votes");
      return electionInstance.candidatos(2);
    }).then(function(candidate2) {
      var voteCount = candidate2[2];
      assert.equal(voteCount, 1, "candidate 2 did not receive any votes");
    });
  });

  it("Confirma voto através de evento", function() {
  return Eleicao.deployed().then(function(instance) {
    electionInstance = instance;
    candidateId = 1;
    return electionInstance.votar(candidatoId, { from: accounts[3] });
  }).then(function(receipt) {
    assert.equal(receipt.logs.length, 1, "an event was triggered");
    assert.equal(receipt.logs[0].event, "eventoConfirmaVoto", "the event type is correct");
    assert.equal(receipt.logs[0].args._candidatoId.toNumber(), candidatoId, "the candidate id is correct");
    return electionInstance.eleitores(accounts[0]);
  }).then(function(voted) {
    assert(voted, "the voter was marked as voted");
    return electionInstance.candidatos(candidateId);
  }).then(function(candidato) {
    var voteCount = candidato[2];
    assert.equal(voteCount, 1, "increments the candidate's vote count");
  })
});

});
